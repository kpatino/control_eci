EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ECI-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X05 P2
U 1 1 554F9D96
P 1100 1700
F 0 "P2" H 1100 2000 50  0000 C CNN
F 1 "BLDC_Motor" V 1200 1700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05" H 1100 1700 60  0001 C CNN
F 3 "" H 1100 1700 60  0000 C CNN
	1    1100 1700
	-1   0    0    1   
$EndComp
Text Label 1450 1900 0    60   ~ 0
BAT+
Wire Wire Line
	1450 1900 1300 1900
Text Label 1450 1800 0    60   ~ 0
BAT-
Wire Wire Line
	1450 1800 1300 1800
Text Label 1450 1700 0    60   ~ 0
SP_IN
Text Label 1450 1600 0    60   ~ 0
SP_OUT
Text Label 1450 1500 0    60   ~ 0
DIR
Wire Wire Line
	1300 1500 1450 1500
Wire Wire Line
	1300 1600 1450 1600
Wire Wire Line
	1300 1700 1450 1700
$Comp
L VCC #PWR029
U 1 1 554F9F2A
P 2300 800
F 0 "#PWR029" H 2300 650 50  0001 C CNN
F 1 "VCC" H 2300 950 50  0000 C CNN
F 2 "" H 2300 800 60  0000 C CNN
F 3 "" H 2300 800 60  0000 C CNN
	1    2300 800 
	1    0    0    -1  
$EndComp
$Comp
L R R17
U 1 1 554F9F30
P 2300 1000
F 0 "R17" V 2380 1000 50  0000 C CNN
F 1 "4.7k" V 2300 1000 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" V 2230 1000 30  0001 C CNN
F 3 "" H 2300 1000 30  0000 C CNN
	1    2300 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 800  2300 850 
Text Label 2300 1300 2    60   ~ 0
DIR
Wire Wire Line
	2300 1300 2300 1150
Text HLabel 2450 1300 2    60   Input ~ 0
DIR_MyDAQ
Wire Wire Line
	2300 1300 2450 1300
Text Label 2300 1600 2    60   ~ 0
SP_OUT
Text Label 2300 1700 2    60   ~ 0
SP_IN
Text HLabel 2400 1600 2    60   Input ~ 0
SP_OUT_MyDAQ
Wire Wire Line
	2300 1600 2400 1600
Text HLabel 2400 1700 2    60   Input ~ 0
SP_MyDAQ
Wire Wire Line
	2400 1700 2300 1700
$Comp
L CONN_01X02 P3
U 1 1 554FA04A
P 1100 2300
F 0 "P3" H 1100 2450 50  0000 C CNN
F 1 "BAT" V 1200 2300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 1100 2300 60  0001 C CNN
F 3 "" H 1100 2300 60  0000 C CNN
	1    1100 2300
	-1   0    0    1   
$EndComp
Text Label 1400 2250 0    60   ~ 0
BAT-
Text Label 1400 2350 0    60   ~ 0
BAT+
Wire Wire Line
	1400 2350 1300 2350
Wire Wire Line
	1400 2250 1300 2250
$EndSCHEMATC
