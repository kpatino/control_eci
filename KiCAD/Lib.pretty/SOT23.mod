<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="http://projects.qi-hardware.com/media/idf/css/yui.css" />
  <link rel="stylesheet" type="text/css" href="http://projects.qi-hardware.com/media/idf/css/style.css" />
  <!--[if lt IE 7]>
  <link rel="stylesheet" type="text/css" href="http://projects.qi-hardware.com/media/idf/css/ie6.css" />
  <![endif]-->
  <link rel="stylesheet" type="text/css" href="http://projects.qi-hardware.com/media/idf/css/prettify.css" />
  <link rel="stylesheet" type="text/css" href="http://downloads.qi-hardware.com/css/style.css" />
  <title>Kicad/kicad_models/SOT23.mod - Hardware Design: SIE Git Source Tree - NanoNote derived board with Jz4725 and FPGA for hardware hackers</title>
</head>
<body>
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content">
<div id="bodyContent">

<div id="doc3" class="yui-t1">
  <div id="hd">
<h1 class="project-title">Hardware Design: SIE</h1>
    <p class="top"><a href="#title" accesskey="2"></a>
<a href="/index.php/login/">Sign in or create your account</a>
 | <a href="/index.php/">Project List</a>

| <a href="/index.php/help/" title="Help and accessibility features">Help</a>
    </p>
<div id="header">
<div id="main-tabs">

  <a accesskey="1" href="/index.php/p/nn-usb-fpga/">Project Home</a> 
  <a href="/index.php/p/nn-usb-fpga/downloads/">Downloads</a> 
  <a href="/index.php/p/nn-usb-fpga/doc/">Documentation</a> 
 <a href="/index.php/p/nn-usb-fpga/issues/">Issues</a>
 <a href="/index.php/p/nn-usb-fpga/source/tree/master/" class="active">Source</a>
 <a href="/index.php/p/nn-usb-fpga/review/">Code Review</a>

</div>


<div id="sub-tabs">
<a class="active" href="/index.php/p/nn-usb-fpga/source/tree/master/">Source Tree</a> |
<a href="/index.php/p/nn-usb-fpga/source/changes/master/">Change Log</a>
 |
<a href="/index.php/p/nn-usb-fpga/source/help/">How To Get The Code</a>
</div>

</div>

	  <h1 class="title" id="title">Hardware Design: SIE Git Source Tree</h1>

  </div>
  <div id="bd">
    <div id="yui-main"> 
      <div class="yui-b">
	<div class="yui-g"> 
          
	  <div class="content">
<h2 class="top"><a href="/index.php/p/nn-usb-fpga/source/tree/978a5556eb2aa605dcccfe0d39b343dcd151df39/">Root</a><span class="sep">/</span><span class="breadcrumb"><a href="/index.php/p/nn-usb-fpga/source/tree/978a5556eb2aa605dcccfe0d39b343dcd151df39/Kicad">Kicad</a><span class="sep">/</span><a href="/index.php/p/nn-usb-fpga/source/tree/978a5556eb2aa605dcccfe0d39b343dcd151df39/Kicad/kicad_models">kicad_models</a><span class="sep">/</span><a href="/index.php/p/nn-usb-fpga/source/tree/978a5556eb2aa605dcccfe0d39b343dcd151df39/Kicad/kicad_models/SOT23.mod">SOT23.mod</a></span></h2>

<table class="code" summary=" ">

 
<tfoot>
<tr><th colspan="2">Source at commit <a class="mono" href="/index.php/p/nn-usb-fpga/source/commit/978a5556eb2aa605dcccfe0d39b343dcd151df39/">978a5556eb2aa605dcccfe0d39b343dcd151df39</a> created 4 years 11 months  ago.<br/>
<span class="smaller">By Juan64Bits, Fixing expansion board for sakc.</span>
</th></tr>
</tfoot>

<tbody>
<tr class="c-line"><td class="code-lc" id="L1"><a href="#L1">1</a></td><td class="code mono">PCBNEW-LibModule-V1  3/9/2009-00:58:50</td></tr>
<tr class="c-line"><td class="code-lc" id="L2"><a href="#L2">2</a></td><td class="code mono">$INDEX</td></tr>
<tr class="c-line"><td class="code-lc" id="L3"><a href="#L3">3</a></td><td class="code mono">SOT23</td></tr>
<tr class="c-line"><td class="code-lc" id="L4"><a href="#L4">4</a></td><td class="code mono">$EndINDEX</td></tr>
<tr class="c-line"><td class="code-lc" id="L5"><a href="#L5">5</a></td><td class="code mono">$MODULE SOT23</td></tr>
<tr class="c-line"><td class="code-lc" id="L6"><a href="#L6">6</a></td><td class="code mono">Po 0 0 0 15 4A9F14BB 4A9F140D ~~</td></tr>
<tr class="c-line"><td class="code-lc" id="L7"><a href="#L7">7</a></td><td class="code mono">Li SOT23</td></tr>
<tr class="c-line"><td class="code-lc" id="L8"><a href="#L8">8</a></td><td class="code mono">Kw SOT23_5</td></tr>
<tr class="c-line"><td class="code-lc" id="L9"><a href="#L9">9</a></td><td class="code mono">Sc 4A9F140D</td></tr>
<tr class="c-line"><td class="code-lc" id="L10"><a href="#L10">10</a></td><td class="code mono">AR /4A4492C8</td></tr>
<tr class="c-line"><td class="code-lc" id="L11"><a href="#L11">11</a></td><td class="code mono">Op 0 0 0</td></tr>
<tr class="c-line"><td class="code-lc" id="L12"><a href="#L12">12</a></td><td class="code mono">T0 0 -1424 270 270 0 30 N I 21 &quot;SOT23&quot;</td></tr>
<tr class="c-line"><td class="code-lc" id="L13"><a href="#L13">13</a></td><td class="code mono">T1 0 368 197 197 0 30 N V 21 &quot;VAL&quot;</td></tr>
<tr class="c-line"><td class="code-lc" id="L14"><a href="#L14">14</a></td><td class="code mono">DS -200 -230 -500 -400 50 21</td></tr>
<tr class="c-line"><td class="code-lc" id="L15"><a href="#L15">15</a></td><td class="code mono">DS 500 -230 -525 -230 50 21</td></tr>
<tr class="c-line"><td class="code-lc" id="L16"><a href="#L16">16</a></td><td class="code mono">DS -525 -230 -525 -770 50 21</td></tr>
<tr class="c-line"><td class="code-lc" id="L17"><a href="#L17">17</a></td><td class="code mono">DS -525 -770 500 -770 50 21</td></tr>
<tr class="c-line"><td class="code-lc" id="L18"><a href="#L18">18</a></td><td class="code mono">DS 500 -770 500 -230 50 21</td></tr>
<tr class="c-line"><td class="code-lc" id="L19"><a href="#L19">19</a></td><td class="code mono">$PAD</td></tr>
<tr class="c-line"><td class="code-lc" id="L20"><a href="#L20">20</a></td><td class="code mono">Sh &quot;3&quot; R 315 315 0 0 0</td></tr>
<tr class="c-line"><td class="code-lc" id="L21"><a href="#L21">21</a></td><td class="code mono">Dr 0 0 0</td></tr>
<tr class="c-line"><td class="code-lc" id="L22"><a href="#L22">22</a></td><td class="code mono">At SMD N 00888000</td></tr>
<tr class="c-line"><td class="code-lc" id="L23"><a href="#L23">23</a></td><td class="code mono">Ne 96 &quot;/PWEN&quot;</td></tr>
<tr class="c-line"><td class="code-lc" id="L24"><a href="#L24">24</a></td><td class="code mono">Po 0 -1000</td></tr>
<tr class="c-line"><td class="code-lc" id="L25"><a href="#L25">25</a></td><td class="code mono">$EndPAD</td></tr>
<tr class="c-line"><td class="code-lc" id="L26"><a href="#L26">26</a></td><td class="code mono">$PAD</td></tr>
<tr class="c-line"><td class="code-lc" id="L27"><a href="#L27">27</a></td><td class="code mono">Sh &quot;2&quot; R 315 315 0 0 0</td></tr>
<tr class="c-line"><td class="code-lc" id="L28"><a href="#L28">28</a></td><td class="code mono">Dr 0 0 0</td></tr>
<tr class="c-line"><td class="code-lc" id="L29"><a href="#L29">29</a></td><td class="code mono">At SMD N 00888000</td></tr>
<tr class="c-line"><td class="code-lc" id="L30"><a href="#L30">30</a></td><td class="code mono">Ne 10 &quot;+BATT&quot;</td></tr>
<tr class="c-line"><td class="code-lc" id="L31"><a href="#L31">31</a></td><td class="code mono">Po 375 0</td></tr>
<tr class="c-line"><td class="code-lc" id="L32"><a href="#L32">32</a></td><td class="code mono">$EndPAD</td></tr>
<tr class="c-line"><td class="code-lc" id="L33"><a href="#L33">33</a></td><td class="code mono">$PAD</td></tr>
<tr class="c-line"><td class="code-lc" id="L34"><a href="#L34">34</a></td><td class="code mono">Sh &quot;1&quot; R 315 315 0 0 0</td></tr>
<tr class="c-line"><td class="code-lc" id="L35"><a href="#L35">35</a></td><td class="code mono">Dr 0 0 0</td></tr>
<tr class="c-line"><td class="code-lc" id="L36"><a href="#L36">36</a></td><td class="code mono">At SMD N 00888000</td></tr>
<tr class="c-line"><td class="code-lc" id="L37"><a href="#L37">37</a></td><td class="code mono">Ne 99 &quot;/PW_ON_N&quot;</td></tr>
<tr class="c-line"><td class="code-lc" id="L38"><a href="#L38">38</a></td><td class="code mono">Po -375 0</td></tr>
<tr class="c-line"><td class="code-lc" id="L39"><a href="#L39">39</a></td><td class="code mono">$EndPAD</td></tr>
<tr class="c-line"><td class="code-lc" id="L40"><a href="#L40">40</a></td><td class="code mono">$SHAPE3D</td></tr>
<tr class="c-line"><td class="code-lc" id="L41"><a href="#L41">41</a></td><td class="code mono">Na &quot;smd/SOT23_6.wrl&quot;</td></tr>
<tr class="c-line"><td class="code-lc" id="L42"><a href="#L42">42</a></td><td class="code mono">Sc 0.110000 0.110000 0.110000</td></tr>
<tr class="c-line"><td class="code-lc" id="L43"><a href="#L43">43</a></td><td class="code mono">Of 0.000000 0.000000 0.000000</td></tr>
<tr class="c-line"><td class="code-lc" id="L44"><a href="#L44">44</a></td><td class="code mono">Ro 0.000000 0.000000 -180.000000</td></tr>
<tr class="c-line"><td class="code-lc" id="L45"><a href="#L45">45</a></td><td class="code mono">$EndSHAPE3D</td></tr>
<tr class="c-line"><td class="code-lc" id="L46"><a href="#L46">46</a></td><td class="code mono">$EndMODULE  SOT23</td></tr>
<tr class="c-line"><td class="code-lc" id="L47"><a href="#L47">47</a></td><td class="code mono">$EndLIBRARY</td></tr>
<tr class="c-line"><td class="code-lc" id="L48"><a href="#L48">48</a></td><td class="code mono"></td></tr>
</tbody>
</table>

<p class="right soft"><a href="/index.php/p/nn-usb-fpga/source/file/978a5556eb2aa605dcccfe0d39b343dcd151df39/Kicad/kicad_models/SOT23.mod"><img style="vertical-align: text-bottom;" src="http://projects.qi-hardware.com/media/idf/img/package-grey.png" alt="Archive" align="bottom" /></a> <a href="/index.php/p/nn-usb-fpga/source/file/978a5556eb2aa605dcccfe0d39b343dcd151df39/Kicad/kicad_models/SOT23.mod">Download this file</a></p>

</div>
	</div>
      </div>
    </div>
    <div class="yui-b context">
<p><strong>Branches:</strong><br/>


<span class="label"><a href="/index.php/p/nn-usb-fpga/source/tree/master/" class="label">master</a></span><br/>

</p>

</div>
  </div>
  <div id="ft"></div>
</div>
<script type="text/javascript" src="http://projects.qi-hardware.com/media/idf/js/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="http://projects.qi-hardware.com/media/idf/js/jquery.hotkeys.js"></script>
<script type="text/javascript" charset="utf-8">
// <!--
jQuery.hotkeys.add('Shift+h',{disableInInput: true},function (){window.location.href='/index.php/help/';});

jQuery.hotkeys.add('Shift+u',{disableInInput: true},function (){window.location.href='/index.php/p/nn-usb-fpga/timeline/';});
jQuery.hotkeys.add('Shift+a',{disableInInput: true},function (){window.location.href='/index.php/p/nn-usb-fpga/issues/create/';});
jQuery.hotkeys.add('Shift+i',{disableInInput: true},function (){window.location.href='/index.php/p/nn-usb-fpga/issues/';});
jQuery.hotkeys.add('Shift+d',{disableInInput: true},function (){window.location.href='/index.php/p/nn-usb-fpga/downloads/';});
jQuery.hotkeys.add('Shift+o',{disableInInput: true},function (){window.location.href='/index.php/p/nn-usb-fpga/doc/';});
jQuery.hotkeys.add('Shift+s',{disableInInput: true},function (){window.location.href='/index.php/p/nn-usb-fpga/source/tree/master/';});
 //-->
</script>



<script type="text/javascript" src="http://projects.qi-hardware.com/media/idf/js/prettify.js"></script>
<script type="text/javascript">prettyPrint();</script>


<script type="text/javascript" charset="utf-8">
//<![CDATA[
$(document).ready(function(){
	var frag = location.hash;
	if ($('#preview').length) {
		location.hash = '#preview';
	}
	else if (frag.length > 3 && frag.substring(0, 3) == '#ic') {
		$(frag).addClass("issue-comment-focus");
	}
});
//]]>
</script>
<br/><br/>

<div id='catlinks'></div>
<div class="visualClear"></div>

</div> <!-- /bodyContent -->
</div> <!-- /content -->

<!-- panel -->
<div id="mw-panel" class="noprint">
	<div id="p-logo"><a style="background-image: url(http://en.qi-hardware.com/wiki-logo.png);" href="http://en.qi-hardware.com/wiki/Main_Page"  title="Visit the main page"></a></div>

<!-- navigation -->
<div class="portal" id='p-navigation'>
	<div class="body">
		<ul>
		<li id="n-mainpage-description"><a href="http://en.qi-hardware.com/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li>
		<li id="n-currentevents"><a href="http://en.qi-hardware.com/wiki/Qi-Hardware:Current_events" title="Find background information on current events">Current events</a></li>
		<li id="n-Mailing-lists"><a href="http://lists.en.qi-hardware.com/mailman/listinfo/">Mailing lists</a></li>
		<li id="n-Planet"><a href="http://en.qi-hardware.com/planet/">Planet</a></li>
		<li id="n-Projects-server"><a href="http://projects.qi-hardware.com/">Projects server</a></li>
		</ul>
	</div>
</div> <!-- /navigation -->

<!-- interactive -->
<div class="portal" id='p-interactive'>
	<h5>interactive</h5>

	<div class="body">
		<ul>
		<li id="n-About-Qi"><a href="http://en.qi-hardware.com/wiki/Qi-Hardware:About">About Qi</a></li>
		<li id="n-recentchanges"><a href="http://en.qi-hardware.com/wiki/Special:RecentChanges" title="The list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li>
		<li id="n-Contact-Qi"><a href="http://en.qi-hardware.com/wiki/Qi-Hardware:Contact_us">Contact Qi</a></li>
		<li id="n-help"><a href="http://en.qi-hardware.com/wiki/Help:Contents" title="The place to find out">Help</a></li>
		</ul>
	</div>
</div> <!-- /interactive -->

</div> <!-- /panel -->

<div id="footer">
	<ul id="footer-info">
	<li id="footer-info-copyright">All content is dual licensed under CC-BY-SA and GFDL; additional terms may apply. See <a href="http://en.qi-hardware.com/wiki/Qi-Hardware:Copyright">Copyright</a> for details.</li>
	</ul>

	<ul id="footer-places">
	<li id="footer-places-privacy"><a href="http://en.qi-hardware.com/wiki/Qi-Hardware:Privacy_policy" title="Qi-Hardware:Privacy policy">Privacy policy</a></li>
	<li id="footer-places-about"><a href="http://en.qi-hardware.com/wiki/Qi-Hardware:About" title="Qi-Hardware:About">About Qi</a></li>
	<li id="footer-places-disclaimer"><a href="http://en.qi-hardware.com/wiki/Qi-Hardware:General_disclaimer" title="Qi-Hardware:General disclaimer">Disclaimers</a></li>
	</ul>

	<ul id="footer-icons" class="noprint">
	<li id="footer-icon-copyright"><a href="http://en.qi-hardware.com/wiki/Qi-Hardware:Copyright"><img src="http://en.qi-hardware.com/cc-by-sa-88x31.png" alt="" width="88" height="31" /></a></li>
	</ul>
<div style="clear:both"></div>
</div> <!-- /footer -->

</body>
</html>
