EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ECI-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X20 P4
U 1 1 55502588
P 1200 3150
F 0 "P4" H 1200 4200 50  0000 C CNN
F 1 "MYDAQ" V 1300 3150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x20" H 1200 3150 60  0001 C CNN
F 3 "" H 1200 3150 60  0000 C CNN
	1    1200 3150
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR030
U 1 1 5550263C
P 1500 2050
F 0 "#PWR030" H 1500 1900 50  0001 C CNN
F 1 "VCC" H 1500 2200 50  0000 C CNN
F 2 "" H 1500 2050 60  0000 C CNN
F 3 "" H 1500 2050 60  0000 C CNN
	1    1500 2050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR031
U 1 1 55502684
P 1650 2150
F 0 "#PWR031" H 1650 1900 50  0001 C CNN
F 1 "GND" H 1650 2000 50  0000 C CNN
F 2 "" H 1650 2150 60  0000 C CNN
F 3 "" H 1650 2150 60  0000 C CNN
	1    1650 2150
	-1   0    0    1   
$EndComp
Wire Wire Line
	1500 2050 1500 2200
Wire Wire Line
	1500 2200 1400 2200
Wire Wire Line
	1650 2150 1650 2300
Wire Wire Line
	1650 2300 1400 2300
Wire Wire Line
	1400 2400 1700 2400
Wire Wire Line
	1400 2500 1700 2500
Wire Wire Line
	1400 2600 1700 2600
Wire Wire Line
	1400 2700 1700 2700
Wire Wire Line
	1400 2800 1700 2800
Wire Wire Line
	1400 2900 1700 2900
Wire Wire Line
	1400 3000 1700 3000
Wire Wire Line
	1400 3100 1700 3100
Wire Wire Line
	1400 3300 1700 3300
Wire Wire Line
	1400 3500 1700 3500
Wire Wire Line
	1400 3700 1700 3700
Wire Wire Line
	1400 3800 1700 3800
NoConn ~ 1400 4000
NoConn ~ 1400 4100
$Comp
L GND #PWR032
U 1 1 5550285C
P 2300 3200
F 0 "#PWR032" H 2300 2950 50  0001 C CNN
F 1 "GND" H 2300 3050 50  0000 C CNN
F 2 "" H 2300 3200 60  0000 C CNN
F 3 "" H 2300 3200 60  0000 C CNN
	1    2300 3200
	-1   0    0    1   
$EndComp
Text HLabel 1700 2400 2    60   Input ~ 0
DIO7
Text HLabel 1700 2500 2    60   Input ~ 0
DIO6
Text HLabel 1700 2600 2    60   Input ~ 0
DIO5
Text HLabel 1700 2700 2    60   Input ~ 0
DIO4
Text HLabel 1700 2800 2    60   Input ~ 0
DIO3
Text HLabel 1700 2900 2    60   Input ~ 0
DIO2
Text HLabel 1700 3000 2    60   Input ~ 0
DIO1
Text HLabel 1700 3100 2    60   Input ~ 0
DIO0
Text HLabel 1700 3500 2    60   Input ~ 0
AI0+
Text HLabel 1700 3300 2    60   Input ~ 0
AI1+
Text HLabel 1700 3800 2    60   Input ~ 0
AO0+
Text HLabel 1700 3700 2    60   Input ~ 0
AO1+
Wire Wire Line
	1400 3200 2300 3200
Wire Wire Line
	1400 3400 2300 3400
Wire Wire Line
	2300 3200 2300 3900
Wire Wire Line
	2300 3600 1400 3600
Connection ~ 2300 3400
Wire Wire Line
	2300 3900 1400 3900
Connection ~ 2300 3600
$EndSCHEMATC
