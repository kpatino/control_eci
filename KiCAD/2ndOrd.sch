EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ECI-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L VCC #PWR017
U 1 1 5547F8B4
P 3900 1400
F 0 "#PWR017" H 3950 1450 60  0001 C CNN
F 1 "VCC" H 3900 1400 60  0001 C CNN
F 2 "" H 3900 1400 60  0001 C CNN
F 3 "" H 3900 1400 60  0000 C CNN
F 4 "#PWR" H 3900 1250 50  0001 C CNN "Reference"
F 5 "VCC" H 3900 1550 50  0000 C CNN "Value"
F 6 "" H 3900 1400 60  0000 C CNN "Footprint"
	1    3900 1400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR018
U 1 1 5547F8D2
P 3900 1950
F 0 "#PWR018" H 3950 2000 60  0001 C CNN
F 1 "GND" H 3900 1950 60  0001 C CNN
F 2 "" H 3900 1950 60  0001 C CNN
F 3 "" H 3900 1950 60  0000 C CNN
F 4 "#PWR" H 3900 1700 50  0001 C CNN "Reference"
F 5 "GND" H 3900 1800 50  0000 C CNN "Value"
F 6 "" H 3900 1950 60  0000 C CNN "Footprint"
	1    3900 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1550 3900 1400
Wire Wire Line
	3900 1850 3900 1950
$Comp
L LMV344 U5
U 1 1 55481A0B
P 5600 1800
F 0 "U5" H 5650 1850 60  0001 C CNN
F 1 "LMV344" H 5600 1800 60  0001 C CNN
F 2 "SMD_Packages:SOIC-14_N" H 5600 1800 60  0001 C CNN
F 3 "" H 5600 1800 60  0000 C CNN
F 4 "U" V 5650 2150 60  0000 C CNN "Reference"
F 5 "LMV344" V 5650 1850 60  0000 C CNN "Value"
F 6 "" H 5600 1800 60  0000 C CNN "Footprint"
	1    5600 1800
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR019
U 1 1 554844B3
P 4950 1250
F 0 "#PWR019" H 5000 1300 60  0001 C CNN
F 1 "VCC" H 4950 1250 60  0001 C CNN
F 2 "" H 4950 1250 60  0001 C CNN
F 3 "" H 4950 1250 60  0000 C CNN
F 4 "#PWR" H 4950 1100 50  0001 C CNN "Reference"
F 5 "VCC" H 4950 1400 50  0000 C CNN "Value"
F 6 "" H 4950 1250 60  0000 C CNN "Footprint"
	1    4950 1250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 554844D2
P 4850 1350
F 0 "#PWR020" H 4900 1400 60  0001 C CNN
F 1 "GND" H 4850 1350 60  0001 C CNN
F 2 "" H 4850 1350 60  0001 C CNN
F 3 "" H 4850 1350 60  0000 C CNN
F 4 "#PWR" H 4850 1100 50  0001 C CNN "Reference"
F 5 "GND" H 4850 1200 50  0000 C CNN "Value"
F 6 "" H 4850 1350 60  0000 C CNN "Footprint"
	1    4850 1350
	0    1    1    0   
$EndComp
Wire Wire Line
	5050 1350 4850 1350
Wire Wire Line
	5050 1250 4950 1250
$Comp
L GND #PWR021
U 1 1 554846DC
P 8950 1850
F 0 "#PWR021" H 9000 1900 60  0001 C CNN
F 1 "GND" H 8950 1850 60  0001 C CNN
F 2 "" H 8950 1850 60  0001 C CNN
F 3 "" H 8950 1850 60  0000 C CNN
F 4 "#PWR" H 8950 1600 50  0001 C CNN "Reference"
F 5 "GND" H 8950 1700 50  0000 C CNN "Value"
F 6 "" H 8950 1850 60  0000 C CNN "Footprint"
	1    8950 1850
	1    0    0    -1  
$EndComp
Text HLabel 3000 3250 0    60   Input ~ 0
OUT2nd1myDAQ
Text Label 4900 2050 2    60   ~ 0
2out
Text Label 3150 3250 0    60   ~ 0
2out
Wire Wire Line
	3000 3250 3150 3250
Wire Wire Line
	4900 2050 5050 2050
Wire Wire Line
	8250 1050 8250 1350
Connection ~ 8250 1350
Wire Wire Line
	8750 1350 9250 1350
Wire Wire Line
	8950 1450 8950 1350
Connection ~ 8950 1350
Wire Wire Line
	8950 1750 8950 1850
Wire Wire Line
	5050 1950 5000 1950
Wire Wire Line
	5000 1950 5000 2050
Connection ~ 5000 2050
Wire Wire Line
	5050 1600 5000 1600
Wire Wire Line
	5000 1600 5000 1500
Wire Wire Line
	5000 1500 5050 1500
Text Label 4950 1700 2    60   ~ 0
1in+
Wire Wire Line
	5050 1700 4950 1700
Text HLabel 3000 3450 0    60   Input ~ 0
myDAQIN2nd1
Text Label 3150 3450 0    60   ~ 0
myDAQIN2nd1
Wire Wire Line
	3150 3450 3000 3450
Text Label 4900 1550 2    60   ~ 0
1out
Wire Wire Line
	4900 1550 5000 1550
Connection ~ 5000 1550
Wire Wire Line
	8100 1350 8450 1350
Wire Wire Line
	8250 1050 8650 1050
Wire Wire Line
	8950 1050 9250 1050
$Comp
L GND #PWR022
U 1 1 55485BF7
P 8200 3000
F 0 "#PWR022" H 8250 3050 60  0001 C CNN
F 1 "GND" H 8200 3000 60  0001 C CNN
F 2 "" H 8200 3000 60  0001 C CNN
F 3 "" H 8200 3000 60  0000 C CNN
F 4 "#PWR" H 8200 2750 50  0001 C CNN "Reference"
F 5 "GND" H 8200 2850 50  0000 C CNN "Value"
F 6 "" H 8200 3000 60  0000 C CNN "Footprint"
	1    8200 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 2950 8200 3000
Wire Wire Line
	8200 2650 8200 2500
Wire Wire Line
	8100 2500 8400 2500
Text Label 8850 2500 0    60   ~ 0
4out
Wire Wire Line
	8850 2500 8700 2500
Text Label 9250 1350 0    60   ~ 0
4in+
Text Label 9250 1050 0    60   ~ 0
4out
Text Label 8100 2500 2    60   ~ 0
4in-
Connection ~ 8200 2500
Text Label 6350 1350 0    60   ~ 0
4in-
Text Label 6350 1250 0    60   ~ 0
4out
Text Label 6350 1450 0    60   ~ 0
4in+
Wire Wire Line
	6250 1450 6350 1450
Wire Wire Line
	6250 1350 6350 1350
Wire Wire Line
	6250 1250 6350 1250
NoConn ~ 6250 1600
NoConn ~ 6250 1700
NoConn ~ 6250 1800
Text HLabel 3000 3700 0    60   Input ~ 0
OUT2nd2myDAQ
Text HLabel 3000 3900 0    60   Input ~ 0
myDAQIN2nd2
Text Label 3150 3700 0    60   ~ 0
4out
Text Label 3150 3900 0    60   ~ 0
myDAQIN2nd2
Wire Wire Line
	3150 3700 3000 3700
Wire Wire Line
	3000 3900 3150 3900
$Comp
L GND #PWR023
U 1 1 5548794E
P 2150 2500
F 0 "#PWR023" H 2200 2550 60  0001 C CNN
F 1 "GND" H 2150 2500 60  0001 C CNN
F 2 "" H 2150 2500 60  0001 C CNN
F 3 "" H 2150 2500 60  0000 C CNN
F 4 "#PWR" H 2150 2250 50  0001 C CNN "Reference"
F 5 "GND" H 2150 2350 50  0000 C CNN "Value"
F 6 "" H 2150 2500 60  0000 C CNN "Footprint"
	1    2150 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 1100 1750 1400
Connection ~ 1750 1400
Wire Wire Line
	1950 2000 2450 2000
Wire Wire Line
	2150 2100 2150 2000
Connection ~ 2150 2000
Wire Wire Line
	2150 2400 2150 2500
Wire Wire Line
	1050 1400 1300 1400
Wire Wire Line
	1750 1100 2150 1100
Wire Wire Line
	2450 1100 2750 1100
Text Label 2450 2000 0    60   ~ 0
2in+
Text Label 2750 1100 0    60   ~ 0
2out
Text Label 2000 1400 0    60   ~ 0
1in+
Wire Wire Line
	1600 1400 2000 1400
Text Label 4950 1850 2    60   ~ 0
2in+
Wire Wire Line
	5050 1850 4950 1850
Text Label 1500 2000 2    60   ~ 0
1out
Wire Wire Line
	1500 2000 1650 2000
Text Label 7700 1350 2    60   ~ 0
myDAQIN2nd2
Text Label 5650 2900 0    60   ~ 0
IN2nd1
Text Label 1050 1400 2    60   ~ 0
IN2nd1
Wire Wire Line
	5650 2650 5650 2900
Text Label 5300 2550 2    60   ~ 0
out_Cascda
Wire Wire Line
	5300 2550 5400 2550
Text Label 6000 2550 0    60   ~ 0
myDAQIN2nd1
Wire Wire Line
	6000 2550 5900 2550
Text Label 10350 1250 2    60   ~ 0
out_Cascda
Text Label 10500 1250 0    60   ~ 0
4out
Wire Wire Line
	10350 1250 10500 1250
Wire Wire Line
	7700 1350 7800 1350
$Comp
L R R7
U 1 1 554FC79E
P 1450 1400
F 0 "R7" V 1530 1400 50  0000 C CNN
F 1 "10k" V 1450 1400 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" V 1380 1400 30  0001 C CNN
F 3 "" H 1450 1400 30  0000 C CNN
	1    1450 1400
	0    1    1    0   
$EndComp
$Comp
L R R8
U 1 1 554FC82F
P 1800 2000
F 0 "R8" V 1880 2000 50  0000 C CNN
F 1 "16k" V 1800 2000 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" V 1730 2000 30  0001 C CNN
F 3 "" H 1800 2000 30  0000 C CNN
	1    1800 2000
	0    1    1    0   
$EndComp
$Comp
L C C6
U 1 1 554FC8E8
P 2300 1100
F 0 "C6" H 2325 1200 50  0000 L CNN
F 1 "0.1u" H 2325 1000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 2338 950 30  0001 C CNN
F 3 "" H 2300 1100 60  0000 C CNN
	1    2300 1100
	0    1    1    0   
$EndComp
$Comp
L C C5
U 1 1 554FCA31
P 2150 2250
F 0 "C5" H 2175 2350 50  0000 L CNN
F 1 "0.01u" H 2175 2150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 2188 2100 30  0001 C CNN
F 3 "" H 2150 2250 60  0000 C CNN
	1    2150 2250
	1    0    0    -1  
$EndComp
$Comp
L C C7
U 1 1 554FCAD1
P 3900 1700
F 0 "C7" H 3925 1800 50  0000 L CNN
F 1 "0.01u" H 3925 1600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3938 1550 30  0001 C CNN
F 3 "" H 3900 1700 60  0000 C CNN
	1    3900 1700
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 554FD04D
P 7950 1350
F 0 "R9" V 8030 1350 50  0000 C CNN
F 1 "9k" V 7950 1350 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" V 7880 1350 30  0001 C CNN
F 3 "" H 7950 1350 30  0000 C CNN
	1    7950 1350
	0    1    1    0   
$EndComp
$Comp
L R R12
U 1 1 554FD091
P 8600 1350
F 0 "R12" V 8680 1350 50  0000 C CNN
F 1 "16k" V 8600 1350 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" V 8530 1350 30  0001 C CNN
F 3 "" H 8600 1350 30  0000 C CNN
	1    8600 1350
	0    1    1    0   
$EndComp
$Comp
L C C8
U 1 1 554FD0B6
P 8800 1050
F 0 "C8" H 8825 1150 50  0000 L CNN
F 1 "0.01u" H 8825 950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 8838 900 30  0001 C CNN
F 3 "" H 8800 1050 60  0000 C CNN
	1    8800 1050
	0    1    1    0   
$EndComp
$Comp
L C C9
U 1 1 554FD0E6
P 8950 1600
F 0 "C9" H 8975 1700 50  0000 L CNN
F 1 "0.01u" H 8975 1500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 8988 1450 30  0001 C CNN
F 3 "" H 8950 1600 60  0000 C CNN
	1    8950 1600
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 554FD10B
P 8550 2500
F 0 "R11" V 8630 2500 50  0000 C CNN
F 1 "18k" V 8550 2500 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" V 8480 2500 30  0001 C CNN
F 3 "" H 8550 2500 30  0000 C CNN
	1    8550 2500
	0    -1   -1   0   
$EndComp
$Comp
L R R10
U 1 1 554FD13D
P 8200 2800
F 0 "R10" V 8280 2800 50  0000 C CNN
F 1 "10k" V 8200 2800 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" V 8130 2800 30  0001 C CNN
F 3 "" H 8200 2800 30  0000 C CNN
	1    8200 2800
	1    0    0    -1  
$EndComp
$Comp
L JUMPER3 JP1
U 1 1 55502420
P 5650 2550
F 0 "JP1" H 5700 2450 50  0000 L CNN
F 1 "JUMPER3" H 5650 2650 50  0000 C BNN
F 2 "Pin_Headers:Pin_Header_Angled_1x03" H 5650 2550 60  0001 C CNN
F 3 "" H 5650 2550 60  0000 C CNN
	1    5650 2550
	1    0    0    -1  
$EndComp
$EndSCHEMATC
