% 2nd-Order system
% Sallen-key Low Pass Filter
%
%                          k/(R1R2C1C2)
%    H=----------------------------------------------
%       s^2 + s*(1/R1C2+1/R3C2+(1-k)/R3C4)+1/(R1R2C1C2)
%
%  Choice for Design Equation
%  R=R1=R3
%  C=C2=C4
%  seta=1/(2*Q);
%


% Input parameters
Q=5;
f0=1e3;
C=0.01e-6; % Selection of C value
RA=10e3;

% Design equation
w0=2*pi*f0;
R=1/(w0*C);
K=3-1/Q;
RB=(K-1)*RA;


num=K*w0^2;
den=[1 w0/Q w0^2];
H=tf(num,den);

step(H);

%%
% 2nd-Order system
% Multi Op Amp Low Pass Filter
%
%                       1/(R1R3C2C4)
%    H=----------------------------------------------
%               s^2 + s*(1/R1C2)+k/(R1R3C2C4)
%
%  Choice for Design Equation
%  C4=m*C2=m*C
%  R3=n*R1=n*R 
%  seta=1/(2*Q);
%


% Input parameters
R=10e3; 
C=0.1e-6;

Q=5; 
f0=1e3;

w0=2*pi*f0; 

m=Q/(w0*R*C);
n=1/(w0*Q*R*C);

C2=C;
C4=m*C2;
R1=R;
R3=n*R1;
K=1;

num=K*w0^2;
den=[1 w0/Q w0^2];
H=tf(num,den);

step(H);



